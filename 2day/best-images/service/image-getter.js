angular.module('imageGetter').factory('imageGetter',function($http,$q) {

var imagesURL = "/images.json";

	function _getImage() {
		var deferred = $q.defer();

		$http.get(imagesURL).then(function(response) {
				var images = response.data.images;
				//var images = response.data.images;
				images.sort(function(a,b){
						return a.score < b.score ? 1 : -1;
					});
				deferred.resolve(images);

			},function(errorresponse) {
				console.error('error in getting ',imagesURL,errorresponse.status);
				deferred.reject(errorresponse);
			});
		return deferred.promise;
	}
	var imageGetter = {
		getImage: _getImage
	};

	return imageGetter;
});
