angular.module('imageGetter').factory('imgGet',function() {

	var _firstName = "jeff";
	var _lastName = "Cohen";
	function _fullName () {
		return _firstName + " " + _lastName;
	}
	var imgGet = {
		name: _firstName,
		fullName: _fullName
	};

	return imgGet;
});
