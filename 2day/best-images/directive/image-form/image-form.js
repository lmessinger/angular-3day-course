angular
    .module('imageGetter').directive("imageForm",function(){
      return {
        restrict: "E",

            controller: function($scope,$http,imageGetter,$interval) {
              //var imagesURL = "/images.json";
                $scope.vm = this;
              //

                    //$scope.vm.imageSrc = images[0].src;


                    imageGetter.getImage().then(function(images){
                      $scope.vm.imageSrc = images[0].src;
                      var i = 1;
                       $interval(function () {
                         $scope.vm.imageSrc = images[i++ % images.length].src;

                       }, 7000);
                    });
                },
        templateUrl: "directive/image-form/image-form.tpl.html"
      };
    });
