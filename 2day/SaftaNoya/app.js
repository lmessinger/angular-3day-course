angular.module('SaftaNoya', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate']);

angular.module('SaftaNoya').config(function($routeProvider) {

    /* Add New Routes Above */
    $routeProvider.otherwise({redirectTo:'/home'});

});

angular.module('SaftaNoya').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});

angular.module('SaftaNoya').controller('grandchildren-viewer',function($scope,grandchildren){
    $scope.allGrandchildren = grandchildren.names;

    $scope.addname = function(name) {
      grandchildren.addGrandchild(name);
    };
});
