var _ = console.log;

console.log("* -----------------");
console.log('* TYPES');
console.log("* -----------------");
console.log('* Numbers');
console.log("* Infinite type");
console.info('1/0:',1/0);
console.log("* parseInt will convert to a number");
console.info('parseInt("12"):',parseInt("12"));
console.log("* NaN is Not a Number");
console.info('parseInt("help")):',parseInt("help"));
console.log("* math utilities are supplied by Math (another built-in object) ");
console.info("Math.sin(3.14/2):",Math.sin(3.14/2));

console.log("* STRINGS");
console.log("* Length");
console.info("length of 'start'","start".length);
console.log("* Methods");
console.info('"start".toUpperCase()',"start".toUpperCase());
console.info('"start".charAt(0):',"start".charAt(0));
console.log("* OTHER TYPES");
// Boolean
console.log("* Boolean is a type, too");
console.info('Boolean(""),Boolean(234):',Boolean(""),Boolean(234));
console.info(" null, undefined:",null,undefined);

console.log("* -----------------");
console.log("* VARIABLES");
console.log("* -----------------");
console.log("* Var is the variable");
var x = 12;
console.info(" x:",x);
console.log("* Uninitizlized is undefined");
var unde;
console.info("unde is:", unde);
console.log("* var can be almost anything");
console.log("* some rules: http://www.codelifter.com/main/tips/tip_020.shtml");

var $,_;
$ = 12;
_  = 'the names';
console.info("$,_:",$,_);

console.log("* The type of the variable is determined on init");
var i = "12",j,s=null;
console.info("typeof i :",typeof i);
console.info(" typeof non-initialized j:",typeof j);
console.info("typeof non-declared a2:",typeof a2);
console.info("typeof s:",typeof s);

console.log("* -----------------");
console.log("* OPERATORS");
console.log("* -----------------");
console.log("* +=, -= etc");
var s1 = "George";
 var a1 = 1;
console.info("s1+=' was here',a1 += 12 + 3:",s1 +=  " was here",a1 += 12 + 3);
console.log("* What happens when there are multiple types?");
console.info('"3" + 4 + 5 :',"3" + 4 + 5);
console.info('3 + 4 + "5":',3 + 4 + "5");

console.log("* COMPARISONS");
console.log("* Can be made with == or === ");
console.info("123 == '123' :",123 == "123");
console.info(" 1 === true:",1 === true);
console.log("* We also have != and !== and bitwise operators");

console.log("* -----------------");
console.log("* FLOW CONTROL");
console.log("* -----------------");

console.log("* while, do");
var a3 = 0;
while (true) {
  // an infinite loop?
  a3++;
  if (a3>30)
    break;
}
console.info(" a3:",a3);

 a3 = "tasty";
do {
  a3 = "very " + a3;

} while (a3.length < 40)
console.info("a3 :","Humus is " + a3);
console.log("* ALSO HAS if, for, switch");

console.log("* Logical shortcuts");
var n,a5;
console.info("n && 'not defined yet' :",n || 'not defined yet');
console.info("a5 && a.name :",a5 && a5.name); // if a5 is defined as an object

console.log("* -----------------");
console.log("* EVAL");
console.log("* -----------------");
console.log("* Evaluates a string to code and runs it");
console.info("eval('for(var i=0;i<10;i++) {var evres=\'I am \'+i;}');",eval('for(var i=0;i<10;i++) {var evres=\'I am \'+i;}'));
console.info(" evres:",evres);
