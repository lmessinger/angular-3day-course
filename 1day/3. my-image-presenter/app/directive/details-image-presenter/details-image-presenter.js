

    angular
        .module('1day').directive("detailsImagePresenter",function(incrementService){
          function incCount() {
            incrementService.inc();
          }
          return {
            restrict: "E",
            replace:false,
            scope: {
              titleColor1:'=',
              ourBossIsTooHardOnUs:'=titleText2'
                },
                controller: function($scope,$element) {
                  incCount();
                  console.log('controller of detailsImagePresenter',$scope.ourBossIsTooHardOnUs);

                  $scope.$watch('ourBossIsTooHardOnUs',function(n,o) {
                    console.log('change ourBossIsTooHardOnUs',n,o)
                  })

                  console.log('h3--->',$element.find("#title2"));
                  $element.on('click',function(target){
                    console.log('click',target);
                  });

                  $scope.$on('$destory',function() {
                    $element.off('click');
                  })

                },
            templateUrl: "directive/details-image-presenter/details-image-presenter.tpl.html"
          }
        });
