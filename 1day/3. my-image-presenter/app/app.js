(function () {
    'use strict';

    //angular.module('1day', ['common', 'shell', 'home', 'help']);
    angular.module('1day', []);

    // RUN: App (module)
    angular
        .module('1day')
        .run(function ($rootScope) {

            $rootScope.safeApply = function (fn) {
                var phase = $rootScope.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    if (fn && (typeof (fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

        });
        


})();
