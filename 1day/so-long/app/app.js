(function () {
    'use strict';

    angular.module('1day', ['common', 'shell', 'home', 'help']);

    // CONFIG: App (module)
    angular
        .module('1day')
        .config(function ($stateProvider, $urlRouterProvider) {

            /* Add New States Above */
            $urlRouterProvider.otherwise('');

        });


    // RUN: App (module)
    angular
        .module('1day')
        .run(function ($rootScope) {

            $rootScope.safeApply = function (fn) {
                var phase = $rootScope.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    if (fn && (typeof (fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

        });
        angular
            .module('1day').controller("endday",function($interval,$scope){
              var vm = this;
              $scope.vm.counter = 10;
              var intPromise = $interval(function() {
                if (!(--vm.counter)) {
                  vm.image = "/images/endoftheworld.png";
                  $interval.cancel(intPromise);
                }
              },1000);

              $scope.$watch('vm.counter',function(n,o){
                vm.counterChnaged = true;
              });
              $scope.$watch('vm.counterChnaged',function(n,o){
                vm.counter++;
              })
            });

})();
