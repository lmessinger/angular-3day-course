(function () {
    'use strict';

    //angular.module('1day', ['common', 'shell', 'home', 'help']);
    angular.module('1day', []);

    // RUN: App (module)
    angular
        .module('1day')
        .run(function ($rootScope) {

            $rootScope.safeApply = function (fn) {
                var phase = $rootScope.$$phase;
                if (phase === '$apply' || phase === '$digest') {
                    if (fn && (typeof (fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

        });
        angular
            .module('1day').controller("endday",function($interval){
              var vm = this;
              vm.counter = 1;
              vm.template = "<div style='font-family:verdana; color: blue'><h3>Image Title</h3>"+
              " <h4>Title:{{vm.titleText}}</h4> "+
              " </div>";
              vm.posx = "0px";
              vm.posy = "0px";
              var intPromise = $interval(function() {
                if (!(--vm.counter)) {
                  vm.image = "/images/endoftheworld.png";
                  $interval.cancel(intPromise);
                }
              },1000)
            });

            angular
                .module('1day').directive("myReallyFlexibleImagePresenter",function($compile,$parse,$interpolate){
                  return {
                    restrict: "A",
                    scope: true,
                      controller: function($scope) {

                      },
                      link: function(scope,element,attrs) {

                        var newscope = scope.$new();
                        var myReallyFlexibleImagePresenter =  $parse(attrs.myReallyFlexibleImagePresenter)(scope);
                        var template = myReallyFlexibleImagePresenter.template ;//scope.$eval(myReallyFlexibleImagePresenter.template);
                        //var posx = scope.$eval(myReallyFlexibleImagePresenter.posx);
                        //var posy = scope.$eval(myReallyFlexibleImagePresenter.posy);
                        var tooltipTemplate = "<div style='position:absolute;padding:20px;border-radius:15px;border:1px dashed blue'>" + template + "</div>";

                        var templateFunction = $compile(tooltipTemplate);


                        var elementPresenter;

                        setTimeout(function() {
                          var htmlContents = templateFunction(newscope);
                          elementPresenter = angular.element(htmlContents);
                          $('body').append(elementPresenter);
                          scope.$apply(function(scope) {
                          scope.vm.titleText = "new name";
                        });
                        },2000);

                        // on mouse over show text div
                        element.on("mouseover",function(e) {
                           scope.$apply(function(scope) {//$digest

                              var htmlContents = templateFunction(newscope);
                              elementPresenter = angular.element(htmlContents);
                              $('body').append(elementPresenter);


                             elementPresenter[0].style.top = "200px";
                             elementPresenter[0].style.left = "200px";
                           });

                        });
                        // on mouse out remove element
                        element.on("mouseout",function(e) {
                          elementPresenter.remove();
                        });

                        // element unbind upon destroying
                        // very important against mem leaks
                        scope.$on('$destroy',function() {
                          element.off();
                        })


                      },
                  }
                });


})();
