describe('start with 2 grandchildren', function() {

  beforeEach(module('SaftaNoya'));

  it('should have 2 grandchildren', inject(function(grandchildren) {

      console.log('cccc');
	   expect(grandchildren.names.length).toEqual(2);

     describe('mazal tov',function() {
         beforeEach(function() {
           grandchildren.addGrandchild('moishik');
         });

         it('should have now 3 grandchildren',function() {
           expect(grandchildren.names.length).toEqual(3);
         });

         it('should have now moishik',function() {
           expect(grandchildren.names[2]).toEqual("moishik");
         });
     });

  }));



});
