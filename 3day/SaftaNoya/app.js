angular.module('SaftaNoya', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate', 'ui.router']);

angular.module('SaftaNoya').config(function($routeProvider) {

    /* Add New Routes Above */
    $routeProvider.otherwise({redirectTo:'/home'});

});

angular.module('SaftaNoya').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

}).
config(function($stateProvider){
  $stateProvider.state("home", {

          // Use a url of "/" to set a state as the "index".
          url: "/",

          template:"<home></home>"
        }).state("home.mazaltov", {

                // Use a url of "/" to set a state as the "index".
                url: "mazaltov/",

                template:"<mazaltov></mazaltov>"
        }).state("home.mishpuche", {

            // Use a url of "/" to set a state as the "index".
            url: "mishpuche/",

            template:"<mishpuche></mishpuche>"
      });


  });
